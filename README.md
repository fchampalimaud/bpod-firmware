# Bpod board firmware #

This repository keeps track of the firmware compatable with [pybpod-api](https://bitbucket.org/fchampalimaud/pybpod-api)

**Note:** The code here is based one the firmware code available on the [original Bpod repository from Sanworks](https://github.com/sanworks/Bpod_Gen2)